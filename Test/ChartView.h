#pragma once

#include <qchart.h>
#include <qchartview.h>

using namespace QtCharts;

class ChartView : public QChartView
{
public:
	using QChartView::QChartView;

	void ScrollEvent(QWheelEvent * event);
	void wheelEvent(QWheelEvent * event);
};
