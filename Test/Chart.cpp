#include "Chart.h"
#include <QtCharts>

Chart::Chart()
{
	m_chart = new QChart;
	m_chartView = new ChartView(m_chart);
}

Chart::~Chart()
{
	delete m_chart;
}

void Chart::ChartInit(Sparam param)
{
	QLineSeries* data = new QLineSeries(m_chart);

	for (int i = 0; i < param.freq.size(); i++)
		data->append(param.freq[i] / pow(10, 9), param.S11Re[i]);

	QColor red("red");
	data->setColor(red);

	m_chart->addSeries(data);
	x = new QValueAxis(m_chart);
	y = new QValueAxis(m_chart);

	x->setTitleText("freq, GHz");
	y->setTitleText("S11");

	x->applyNiceNumbers();

	m_chart->addAxis(x, Qt::AlignBottom);
	data->attachAxis(x);
	m_chart->addAxis(y, Qt::AlignLeft);
	data->attachAxis(y);

	m_chartView->setRenderHint(QPainter::Antialiasing);
}

void Chart::AddLine(Sparam param)
{
	QLineSeries* data = new QLineSeries(m_chart);

	for (int i = 0; i < param.freq.size(); i++)
		 data->append(param.freq[i] / pow(10, 9), param.S11Re[i]);

	QColor blue("blue");
	data->setColor(blue);

	m_chart->addSeries(data);
	data->attachAxis(x);
	data->attachAxis(y);
	m_chartView->setRenderHint(QPainter::Antialiasing);
}

void Chart::AddPoints(Sparam param)
{
	QScatterSeries* data = new QScatterSeries(m_chart);

	for (int i = 0; i < param.freq.size(); i++)
		data->append(param.freq[i], param.S11Re[i]);

	m_chart->addSeries(data);
	data->attachAxis(x);
	data->attachAxis(y);
	m_chartView->setRenderHint(QPainter::Antialiasing);
}
