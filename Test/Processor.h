#pragma once

#define _USE_MATH_DEFINES

#include <vector>
#include <fstream>
#include <cmath>
#include <complex>
#include <valarray>
#include <algorithm>

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;

struct Sparam
{
	std::vector<float> freq;
	std::vector<float> S11Re;
	std::vector<float> S11Im;
};

struct SparamBuffer
{
	SparamBuffer(int size) : param(size), freq(size) {}
	SparamBuffer() = default;
	std::vector<float> param;
	std::vector<float> freq;
};

enum SparamFlag
{
	S11Re,
	S11Im
};

class Processor
{
public:
	Sparam ParceFile(const std::string& filename);
	void WriteFile(std::string filename, const Sparam data);

	std::vector<float> MovingAverage(const std::vector<float>& data, int windowSize);
	void MaxMinAverage(const Sparam& data);
	std::vector<float> Kalman(const std::vector<float>& data, float K);
	std::vector<float> RCchain(const std::vector<float>& data, float f);
	SparamBuffer VectorAverage(const std::vector<float>& data, const std::vector<float>& freq);
	std::vector<float> InterpolatedVectorAverage(const std::vector<float>& data, const std::vector<float>& freq);

	CArray Fourier(const std::vector<float>& data);
	std::vector<float> InverseFourier(CArray x);

	int FindPeriod(const std::vector<float>& data);
	std::vector<int> FindPeriods(const std::vector<float>& data);

	std::vector<Sparam> data;

private:
	float LineInterpolate(float x, float x1, float y1, float x2, float y2);
	void Merge(SparamBuffer&, SparamBuffer&);
	void fft(CArray& dat);
	void ifft(CArray& x);
};

