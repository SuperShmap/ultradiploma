#include "MainWindow.h"


MainWindow::MainWindow(QMainWindow* parent) : QMainWindow(parent)
{
	m_centralWidget = new QWidget(this);
	m_grid = new QGridLayout(this);

	m_menu = new QMenuBar(this);
	m_file = new QMenu("File", this);
	m_actions = new QMenu("Edit", this);
	m_menu->addMenu(m_file);
	m_menu->addMenu(m_actions);

	QAction* open = new QAction("Open file", this);
	QAction* save = new QAction("Save file", this);
	QAction* proceed = new QAction("Proceed", this);
	QAction* fourier = new QAction("Fourier transform", this);

	connect(open, SIGNAL(triggered()), this, SLOT(OpenFile()));
	connect(save, SIGNAL(triggered()), this, SLOT(WriteFile()));
	connect(proceed, SIGNAL(triggered()), this, SLOT(Proceed()));
	connect(fourier, SIGNAL(triggered()), this, SLOT(Fourier()));

	m_file->addAction(open);
	m_file->addAction(save);
	m_actions->addAction(proceed);
	m_actions->addAction(fourier);

	QAction* movingAvg = new QAction("Moving average", this);
	QAction* expFil = new QAction("Exponentional filter", this);
	m_actions->addAction(movingAvg);
	m_actions->addAction(expFil);
}


MainWindow::~MainWindow()
{ 
	//if (m_fourierWindow != nullptr)
	//{
	//	delete m_fourierWindow;
	//	m_fourierWindow = nullptr;
	//}
}

void MainWindow::Init()
{
	this->setCentralWidget(m_centralWidget);

	m_centralWidget->setLayout(m_grid);

	//***********Configure widgets*********
	m_grid->setMenuBar(m_menu);
	m_grid->addWidget(m_chart.m_chartView, 0, 0);

}

void MainWindow::OpenFile()
{
	QFileDialog dialog(this);
	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setFileMode(QFileDialog::ExistingFile);
	
	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setViewMode(QFileDialog::List);

	QStringList filenames;
	if (dialog.exec())
	{
		filenames = dialog.selectedFiles();
		std::string filename = filenames[0].toStdString();
		m_rawData = m_proc.ParceFile(filename.c_str());
	}
	m_chart.ChartInit(m_rawData);
}

void MainWindow::Proceed()
{
	OpenFile();

	m_proceedData.S11Re = m_proc.MovingAverage(m_rawData.S11Re, 20);
	m_proceedData.S11Re = m_proc.InterpolatedVectorAverage(m_proceedData.S11Re, m_rawData.freq);
	m_proceedData.S11Re = m_proc.InterpolatedVectorAverage(m_proceedData.S11Re, m_rawData.freq);
	m_proceedData.freq = m_rawData.freq;
	m_proceedData.S11Re = m_proc.MovingAverage(m_proceedData.S11Re, 4);
	
	m_chart.AddLine(m_proceedData);

	/*
	SparamBuffer S11Re;
	SparamBuffer S11Im;

	//m_proceedData.freq = m_rawData.freq;
	//m_proceedData.S11Re = m_proc.VectorAverage(m_rawData.S11Re, m_rawData.freq);
	//m_proceedData.S11Im = m_proc.VectorAverage(m_rawData.S11Im, m_rawData.freq);
	//m_proceedData.S11Re = m_proc.MovingAverage(m_proceedData.S11Re, 2);
	//m_proceedData.S11Im = m_proc.MovingAverage(S11Im.param, 2);

	//S11Re.param = m_proc.RCchain(m_rawData.S11Re, 0.05);
	S11Re.freq = m_rawData.freq;

	S11Re.param = m_proc.MovingAverage(m_rawData.S11Re, 70);
	S11Re = m_proc.VectorAverage(S11Re.param, S11Re.freq);
	//S11Re.param = m_proc.MovingAverage(S11Re.param, 2);
	
	m_proceedData.freq = S11Re.freq;
	m_proceedData.S11Re = S11Re.param;
	m_proceedData.S11Im.resize(S11Re.freq.size());
	m_chart.AddLine(m_proceedData);
	*/

	//m_proceedData.freq = m_rawData.freq;
	//m_proceedData.S11Re = m_proc.Fourier(m_rawData.S11Re);
	//m_proceedData.S11Im.resize(m_proceedData.freq.size());
	//m_chart.AddLine(m_proceedData);
}



void MainWindow::WriteFile()
{
	QString fileName = QFileDialog::getSaveFileName(this,
		tr("Save file"), "",
		tr("Address Book (*.s2p);;All Files (*)"));

	if (fileName.isEmpty())
		return;
	else
		m_proc.WriteFile(fileName.toStdString(), m_proceedData);
}

void MainWindow::Fourier()
{
	m_fourierWindow = new FourierWindow(m_rawData);

	CArray test;

	test = m_proc.Fourier(m_rawData.S11Re);

	/*bool up = false;
	if (m_rawData.S11Re[0] < m_rawData.S11Re[1])
		up = true;
	int period = 0;
	std::vector<int> periods;

	for (int i = 1; i < m_rawData.S11Re.size(); i++)
	{
		if ((m_rawData.S11Re[i] > m_rawData.S11Re[i - 1]) && !up)
		{
			periods.push_back(i - period);
			period = i;
			up = !up;
		}
		if ((m_rawData.S11Re[i] < m_rawData.S11Re[i - 1]) && up)
			up = !up;
	}

	int size = test.size() - 1;

	for (int i = 0; i < periods.size(); i++)
	{
		if ((periods[i] < size) && (periods[i] > 0))
		{
			test[periods[i]] = 0;
		}
	}*/

	for (int i = 20; i < test.size(); i++)
		test[i] = 0;

	m_proceedData.S11Re = m_proc.InverseFourier(test);
	m_proceedData.freq = m_rawData.freq;
	m_proceedData.S11Im.resize(m_rawData.freq.size());

	m_chart.AddLine(m_proceedData);
}