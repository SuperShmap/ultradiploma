#pragma once

#undef QT_NO_WHEELEVENT

#include <QtCharts\qchart.h>
#include <QtCharts\qchartview.h>
#include <qvalueaxis.h>

#include "Processor.h"

#include "ChartView.h"

using namespace QtCharts;

class Chart : public QWidget
{
	Q_OBJECT
public:
	Chart();
	~Chart();

	void ChartInit(Sparam param);
	void AddLine(Sparam param);

	void AddPoints(Sparam param);

	void MoveEvent(QEvent* wheel);

	QChart* m_chart;
	ChartView* m_chartView;
	QValueAxis* x;
	QValueAxis* y;
};

