#include "Processor.h"
#include <assert.h>


Sparam Processor::ParceFile(const std::string& filename)
{
	Sparam s;

	std::ifstream file;

	file.open(filename, std::ios::in);
	if (file.is_open())
{
	char charBuffer[1024];
	float freq, S11Re, S11Im; //buffer for Sparam structure

	while (!file.eof())
	{
		file.getline(charBuffer, 1024);
		if (!(charBuffer[0] == '!' || charBuffer[0] == '#'))
		{
			sscanf_s(charBuffer, "%f %f %f", &freq, &S11Re, &S11Im);
			s.freq.push_back(freq);
			s.S11Re.push_back(S11Re);
			s.S11Im.push_back(S11Im);
		}
	}
}
	file.close();
	return s;
}

void Processor::WriteFile(std::string filename, const Sparam data)
{
	std::basic_ofstream<char> file;
	file.open(filename, std::ios::out);

	if (file.is_open())
	{
		file << "# Hz S  RI   R 50 \n";
		for (int i = 0; i < data.S11Re.size()-1; i++)
		{
			file << data.freq[i] << "    " << data.S11Re[i] << "    " <<
				data.S11Im[i] << "	0 0 0 0 0 0 " << "\n";
		}
	}

	file.close();
}

void Processor::MaxMinAverage(const Sparam& data)
{
	Sparam resultRe;
	resultRe.freq = data.freq;

	//resultRe.S11Re = Kalman(data.S11Re, 0.02);
	//resultRe.S11Im = Kalman(data.S11Im, 0.02);

	//resultRe.S11Re = VectorAverage(data.S11Re, data.freq);
	//resultRe.S11Im = VectorAverage(data.S11Im, data.freq);

	WriteFile("OutputS11Re.s2p", resultRe);
	//WriteFile("OutputS11Im.s2p", S11Im);
}

std::vector<float> Processor::MovingAverage(const std::vector<float>& data, int windowSize)
{
	std::vector<float> result;

	for (int i = 0; i < windowSize; i++)
		result.push_back(data[i]);

	for (int i = windowSize; i < data.size(); i++)
	{
		float buffer = 0;

		for (int j = (i - windowSize); i >= j; j++)
			buffer += data[j];

		result.push_back(buffer / windowSize);
	}
	return result;
}

SparamBuffer Processor::VectorAverage(const std::vector<float>& data, const std::vector<float>& freq)
{
	SparamBuffer result;

	bool up = false;

	if (data[0] < data[1])
		up = true;

	for (int i = 1; i < data.size(); i++)
	{
		if ((data[i - 1] > data[i]) && !up)
		{
			result.param.push_back(data[i]);
			result.freq.push_back(freq[i]);
			up = !up;
		}
		if ((data[i - 1] < data[i]) && up)
		{
			result.param.push_back(data[i]);
			result.freq.push_back(freq[i]);
			up = !up;
		}
	}

	for (int i = 1; i < result.freq.size(); i++)
	{
		result.freq[i - 1] = (result.freq[i - 1] + result.freq[i]) / 2;
		result.param[i - 1] = (result.param[i - 1] + result.param[i]) / 2;
	}
	return result;
}

float Processor::LineInterpolate(float x, float x1, float y1, float x2, float y2)
{
	return y1 + (x - x1) * (y2 - y1) / (x2 - x1);
}

void Processor::Merge(SparamBuffer& re, SparamBuffer& im)
{
	Sparam result;
	for (int i = 0, j = 0; (i < re.freq.size()) && (j < im.freq.size());)
	{
		if (re.freq[i] < im.freq[j])
		{
			result.freq.push_back(re.freq[i]);
			result.S11Re.push_back(re.param[i]);
			result.S11Im.push_back(NULL);
			i++;
		}
		else 
			if (re.freq[i] > im.freq[j])
		{
			result.freq.push_back(im.freq[j]);
			result.S11Im.push_back(im.param[j]);
			result.S11Re.push_back(NULL);
			j++;
		}
			else
			{
				result.freq.push_back(im.freq[j]);
				result.S11Im.push_back(im.param[j]);
				result.S11Re.push_back(re.param[i]);
				i++;
				j++;
			}
	}

	for (int i = 0; i < result.freq.size(); i++)
	{

	}

}

std::vector<float> Processor::Kalman(const std::vector<float>& data, float K)
{
	std::vector<float> result;
	result.push_back(data[0]);

	for (int i = 1; i < data.size(); i++)
	{
		float x_opt = K * data[i] + (1 - K) * result[i - 1];
		result.push_back(x_opt);
	}
	return result;
}

std::vector<float> Processor::RCchain(const std::vector<float>& data, float f)
{
	float alpha;
	auto tau = static_cast<float>(1 / (2 * M_PI * f));
	alpha = 1 / (1 + 2 * tau);

	std::vector<float> result;
	result.push_back(data[0]);

	for (int i = 1; i < data.size(); i++)
		result.push_back(alpha * (data[i] + data[i - 1]) + (1 - 2 * alpha) * result[i - 1]);

	return result;
}

CArray Processor::Fourier(const std::vector<float>& data)
{
	int N = data.size();

	int power = 0;

	CArray x;
	x.resize(data.size());

	for (int i = 0; i < data.size(); i++)
		x[i] = data[i];

	fft(x);

	return x;
}

std::vector<float> Processor::InverseFourier(CArray x)
{
	std::vector<float> result;
	ifft(x);
	for (int i = 0; i < x.size(); i++)
		result.push_back(static_cast<float>(x[i].real()));

	return result;
}

void Processor::fft(CArray& x)
{
	const size_t N = x.size();
	if (N <= 1) return;

	// divide
	CArray even = x[std::slice(0, N / 2, 2)];
	CArray  odd = x[std::slice(1, N / 2, 2)];

	// conquer
	fft(even);
	fft(odd);

	// combine
	for (size_t k = 0; k < N / 2; ++k)
	{
		Complex t = std::polar(1.0, -2 * M_PI * k / N) * odd[k];
		x[k] = even[k] + t;
		x[k + N / 2] = even[k] - t;
	}
}

void Processor::ifft(CArray& x)
{
	// conjugate the complex numbers
	x = x.apply(std::conj);

	// forward fft
	fft(x);

	// conjugate the complex numbers again
	x = x.apply(std::conj);

	// scale the numbers
	x /= static_cast<double>(x.size());
}

int Processor::FindPeriod(const std::vector<float>& data)
{
	std::vector<int> buffer = FindPeriods(data);
	return buffer[buffer.size() / 2];
}

std::vector<int> Processor::FindPeriods(const std::vector<float>& data)
{
	std::vector<int> buffer;

	bool up = data[0] < data[1];

	for (int i = 1; i < data.size(); i++)
	{
		if ((data[i - 1] > data[i]) && up)
		{
			buffer.push_back(i - 1);
			up = !up;
		}
		if ((data[i - 1] < data[i]) && !up)
			up = !up;
	}

	for (int i = 0; i < buffer.size() - 1; i++)
		buffer[i] = buffer[i + 1] - buffer[i];

	std::sort(buffer.begin(), buffer.end());

	return buffer;
}

std::vector<float> Processor::InterpolatedVectorAverage(const std::vector<float>& data, const std::vector<float>& freq)
{
	SparamBuffer result;

	bool up = false;

	if (data[0] < data[1])
		up = true;

	for (int i = 1; i < data.size(); i++)
	{
		if ((data[i - 1] > data[i]) && !up)
		{
			result.param.push_back(data[i]);
			result.freq.push_back(freq[i]);
			up = !up;
		}
		if ((data[i - 1] < data[i]) && up)
		{
			result.param.push_back(data[i]);
			result.freq.push_back(freq[i]);
			up = !up;
		}
	}

	for (int i = 1; i < result.freq.size(); i++)
	{
		result.freq[i - 1] = (result.freq[i - 1] + result.freq[i]) / 2;
		result.param[i - 1] = (result.param[i - 1] + result.param[i]) / 2;
	}

	std::vector<float> interpolatedResult(data.size());

	int curLeftInterpolatedElemIdx = 0;

	for (int curDataIdx = 0; curDataIdx < interpolatedResult.size(); curDataIdx++)
	{
		if (curLeftInterpolatedElemIdx > result.freq.size() - 2) // if there is no more filtered points
		{
			interpolatedResult[curDataIdx] = interpolatedResult[curDataIdx - 1];
		}
		else
		{
			if (freq[curDataIdx] == result.freq[curLeftInterpolatedElemIdx + 1]) //if frequencies is equal
			{
				interpolatedResult[curDataIdx] = result.param[curLeftInterpolatedElemIdx + 1];
				curLeftInterpolatedElemIdx++;
			}
			else
			{
				if (freq[curDataIdx] > result.freq[curLeftInterpolatedElemIdx + 1]) // if we moved outside the current interval of filtered points
				{
					while (freq[curDataIdx] > result.freq[curLeftInterpolatedElemIdx + 1])
					{
						curLeftInterpolatedElemIdx++;
					}
				}

				interpolatedResult[curDataIdx] = LineInterpolate(freq[curDataIdx],
					result.freq[curLeftInterpolatedElemIdx], result.param[curLeftInterpolatedElemIdx],
					result.freq[curLeftInterpolatedElemIdx + 1], result.param[curLeftInterpolatedElemIdx + 1]);
			}
		}
	}
	return interpolatedResult;
}