#pragma once

#include <valarray>

#include <qmainwindow.h>
#include <qchart.h>
#include <qbarseries.h>
#include <qbarset.h>

#include "Chart.h"
#include "Processor.h"

class FourierWindow : public QMainWindow
{
	Q_OBJECT

public:
	FourierWindow(Sparam proceedData);
	~FourierWindow();

	void ShowChart(CArray data);

	QChart* m_chart;
	Processor m_proc;
};

