
#include "ChartView.h"

void ChartView::ScrollEvent(QWheelEvent* event)
{
	if (event->delta() > 0)
		chart()->zoomIn();
	else
		chart()->zoomOut();
}

void ChartView::wheelEvent(QWheelEvent* event)
{
	QWidget::wheelEvent(event);
	ScrollEvent(event);
}