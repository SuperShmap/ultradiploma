#include "FourierWindow.h"



FourierWindow::FourierWindow(Sparam rawData)
{
	Sparam proceedData;

	CArray x;

	x = m_proc.Fourier(rawData.S11Re);

	ShowChart(x);

	this->show();


	//proceedData.S11Re = m_proc.InverseFourier(proceedData.S11Re, 7002);
	//proceedData.freq = rawData.freq;
	//proceedData.S11Im.resize(rawData.freq.size());
	//
	//m_chart.AddLine(proceedData);
}


FourierWindow::~FourierWindow()
{
	if (m_chart != nullptr)
	{
		delete m_chart;
		m_chart = nullptr;
	}
}

void FourierWindow::ShowChart(CArray data)
{
	m_chart = new QChart;

	QBarSeries* bars = new QBarSeries(m_chart);

	QBarSet* set1 = new QBarSet("freq", m_chart);

	for (int i = 0; i < data.size(); i++)
		set1->append(abs(data[i]));

	bars->append(set1);
	m_chart->addSeries(bars);

	QValueAxis* x = new QValueAxis(m_chart);
	QValueAxis* y = new QValueAxis(m_chart);

	x->setRange(0, 250);
	y->setRange(0, 50);

	x->applyNiceNumbers();

	m_chart->addAxis(x, Qt::AlignBottom);
	bars->attachAxis(x);
	m_chart->addAxis(y, Qt::AlignLeft);
	bars->attachAxis(y);

	QChartView* view = new QChartView(m_chart);
	view->setRenderHint(QPainter::Antialiasing);

	this->setCentralWidget(view);
}