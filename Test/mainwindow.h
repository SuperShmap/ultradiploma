#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#pragma once

#include <complex>
#include <valarray>

#include <qmainwindow.h>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <qgridlayout.h>
#include <qfiledialog.h>
#include <qdialog.h>
#include <qvalidator.h>
#include <qmenubar.h>
#include <qmessagebox.h>

#include <limits>

#include "Processor.h"
#include "Chart.h"
#include "FourierWindow.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit MainWindow(QMainWindow *parent = 0);
	~MainWindow();

	void Init();
	float QStringToFloat();

	QMenuBar* m_menu;
	QMenu* m_file;
	QMenu* m_actions;

	QWidget* m_centralWidget;
	QGridLayout* m_grid;

	Processor m_proc;
	Chart m_chart;

	Sparam m_rawData;
	Sparam m_proceedData;

	FourierWindow* m_fourierWindow;

	public slots:
	void OpenFile();
	void Proceed();
	void WriteFile();
	void Fourier();
};

#endif // MAINWINDOW_H
